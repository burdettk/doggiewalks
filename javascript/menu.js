$( document ).ready(function() {
  $('.hamburger').on('click', function(ev){               // when the hamburger button is clicked on a smaller device, the width of the nav should go to 100%
    ev.preventDefault();
    $('nav').css('width', '100%');
    $('#close').removeClass('hidden');                  // remove the 'x' close button
  });
  $('#close').on('click', function(ev) {            // on click of the 'x' close button, close the nav makind the width 0, and the opened nav hidden
    ev.preventDefault();
    $('nav').css('width', '0');
    $('#close').addClass('hidden');
  });
});
